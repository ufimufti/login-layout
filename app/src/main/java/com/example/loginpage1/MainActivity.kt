package com.example.loginpage1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, BerandaActivity::class.java)
        intent.putExtra(“name”,”Pakde”)
        startActivity(intent)
    }
}